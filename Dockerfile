FROM docker.io/tiangolo/uwsgi-nginx:python3.11

LABEL maintainer="Martin Roukala <martin.roukala@mupuf.org>"

# Copy the website to the /app folder
COPY . /app/

# Install all our dependencies
WORKDIR /app
RUN pip install -r requirements.txt
