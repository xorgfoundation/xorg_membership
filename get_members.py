#!/usr/bin/env python3

# Set up django
import django
django.setup()

if __name__ == '__main__':
    from django.utils import timezone
    from gui.models import MembershipPeriod

    now = timezone.now()

    emails = set()
    for period in MembershipPeriod.objects.filter(start__lt=now, end__gt=now):
        for member in period.members:
            emailaddress = member.user_profile.user.emailaddress_set.filter(primary=True).first()
            if emailaddress is not None:
                emails.add(emailaddress.email)

    for email in emails:
        print(email)
