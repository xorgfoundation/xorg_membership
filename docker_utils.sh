function start_docker() {
    # Loads all the environment variables then start the docker image
    source "$PWD/setup-env.sh"
    docker run --rm -v $PWD/db.sqlite3:/app/db.sqlite3 -p $HOST_PORT:80 \
            -e PRODUCTION=$PRODUCTION -e HOST_URL=$HOST_URL \
            -e SECRET_KEY=$SECRET_KEY -e EMAIL_USE_TLS=$EMAIL_USE_TLS \
            -e EMAIL_HOST=$EMAIL_HOST -e EMAIL_HOST_USER=$EMAIL_HOST_USER \
            -e EMAIL_HOST_PASSWORD=$EMAIL_HOST_PASSWORD -e EMAIL_PORT=$EMAIL_PORT \
            -e EMAIL_FROM=$EMAIL_FROM -e USE_FORWARDED_HEADERS=$USE_FORWARDED_HEADERS \
            -e DJANGO_SETTINGS_MODULE=xorg_membership.settings \
            --name xorg_membership \
            registry.freedesktop.org/xorgfoundation/xorg_membership:latest $@
}
