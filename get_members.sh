#!/bin/bash

# Set the CWD to this script's directory
cd "$(dirname "$(readlink -f "$0")")"

source "$PWD/docker_utils.sh"

start_docker python3 /app/get_members.py
