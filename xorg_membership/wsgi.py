"""
WSGI config for xorg_membership project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from django.core import management
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'xorg_membership.settings')

django.setup()
management.call_command("migrate", interactive=False)
management.call_command("collectstatic", interactive=False)

application = get_wsgi_application()
