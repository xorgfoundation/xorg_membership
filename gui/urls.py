from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('members', views.MembersListView.as_view(), name='members'),
    path('members/<int:pk>/make/admin', views.ChangeUserStatus.as_view(), {"superuser": True},
         name='members-user-to-admin'),
    path('members/<int:pk>/make/user', views.ChangeUserStatus.as_view(), {"superuser": False},
         name='members-admin-to-user'),
    path('about', views.about, name='about'),
    path('account/delete', views.account_delete, name='account-delete'),
    path('account/profile', views.profile, name='account-profile'),
    path('membership/apply', views.membership_application, name='membership-application'),
    path('membership/approve', views.MembershipApplicationListView.as_view(), name='membership-approval'),
    path('membership-period/create', views.MembershipPeriodCreateView.as_view(), name='membership-period-create'),
    path('ballot/', views.BallotListView.as_view(), name='ballot-list'),
    path('ballot/<int:pk>/vote', views.VotingView.as_view(), name='ballot-vote'),
    path('ballot/<int:pk>/admin', views.BallotAdminView.as_view(), name='ballot-admin'),
    path('ballot/<int:pk>/send_reminder', views.BallotSendReminderView.as_view(), name='ballot-send-reminder'),
]
