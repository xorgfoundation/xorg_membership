{% extends 'gui/email/message_base.txt' %}

{% block message %}The administrator '{{admin.get_full_name}}' has changed your role from '{{role}}' to '{{new_role}}'.{% endblock %}
