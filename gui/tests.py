from django.conf import settings
from django.core import mail
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from datetime import timedelta
from unittest.mock import patch, MagicMock, PropertyMock

from allauth.account.models import EmailAddress

from htmlvalidator.client import ValidatingClient

from .models import MembershipPeriod, Membership, Link, Profile, Ballot, BallotSelectQuestion
from .models import BallotSelectQuestionPossibleAnswer, BallotSelectedAnswer, BallotRankingQuestion
from .models import BallotRankingQuestionOption, BallotRankingAnswer, VoteCount, OptionRankingVotes
from .context_processors import admin_tasks_count, global_context
from .forms import ProfileForm, MembershipApplicationForm, BallotForm
from .email import Email, AdminEmail, email_context

from collections import OrderedDict
import logging
import os

logging.disable(logging.CRITICAL)


class EmailTests(TestCase):
    def test_send(self):
        with self.settings(EMAIL_HOST_USER="hello", EMAIL_HOST='me.com', DEFAULT_FROM_EMAIL=None):
            email = Email("my subject", 'the wonderful\nmessage',
                          ['one@domain.tld', 'two@domain.tld', 'three@domain.tld'])
            email.send()

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, email.to)
        self.assertEqual(mail.outbox[0].from_email, 'hello@me.com')
        self.assertEqual(mail.outbox[0].subject, '[X.Org membership] ' + email.subject)
        self.assertEqual(mail.outbox[0].body, email.message)

    def test_send_with_default_from(self):
        with self.settings(DEFAULT_FROM_EMAIL='hello2@me.com'):
            email = Email("my subject", 'the wonderful\nmessage',
                          ['one@domain.tld', 'two@domain.tld', 'three@domain.tld'])
            email.send()

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].from_email, 'hello2@me.com')


class AdminEmailTests(TestCase):
    def test_init(self):
        for i in range(6):
            get_user_model().objects.create_user('user{}'.format(i),
                                                 'user{}@provider.com'.format(i),
                                                 'pwd', is_superuser=((i % 2) == 0))

        email = AdminEmail("my subject", 'the wonderful\nadmin\nmessage')

        self.assertEqual(email.to, ['user0@provider.com', 'user2@provider.com', 'user4@provider.com'])
        self.assertEqual(email.subject, 'my subject')
        self.assertEqual(email.message, 'the wonderful\nadmin\nmessage')


class EmailContextTests(TestCase):
    def test_email_context(self):
        context = email_context(toto="hello")
        self.assertIn('current_site', context)
        self.assertEqual(context.get('toto'), 'hello')


def create_user_and_log_in(client, admin=False, verified=False):
    user = get_user_model().objects.create_user('user', 'user@provider.com', 'pwd',
                                                first_name='First', last_name='Last',
                                                is_superuser=admin)
    EmailAddress.objects.create(user=user, primary=True, verified=verified,
                                email=user.email)
    client.login(username='user', password='pwd')
    return user


class MembershipPeriodTests(TestCase):
    def test_current_period(self):
        # Check that by default, we get no periods at all
        self.assertEqual(MembershipPeriod.current_period(), None)

        # Check that no future period becomes now
        MembershipPeriod.objects.create(short_name="future", description="", agreement_url="https://x.org",
                                        start=timezone.now() + timedelta(hours=1),
                                        end=timezone.now() + timedelta(hours=2))
        self.assertEqual(MembershipPeriod.current_period(), None)

        # Check that the ordering is right for dates in the past
        old = MembershipPeriod.objects.create(short_name="old", description="", agreement_url="https://x.org",
                                              start=timezone.now(),
                                              end=timezone.now() + timedelta(hours=2))
        self.assertEqual(MembershipPeriod.current_period(), old)

        new = MembershipPeriod.objects.create(short_name="new", description="", agreement_url="https://x.org",
                                              start=timezone.now(),
                                              end=timezone.now() + timedelta(hours=2))

        self.assertEqual(MembershipPeriod.current_period(), new)

    @patch('gui.models.Membership.objects.filter')
    def test_members(self, filter_mocked):
        period = MembershipPeriod.objects.create(short_name="period name", start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))

        members = period.members
        filter_mocked.assert_called_with(period=period)
        filter_mocked.return_value.exclude.assert_called_with(approved_on=None, rejected_on=None)
        self.assertEqual(members, filter_mocked.return_value.exclude.return_value)

    def test_str(self):
        start = timezone.now()
        period = MembershipPeriod.objects.create(short_name="period name", start=start,
                                                 end=timezone.now() + timedelta(hours=2))
        self.assertEqual(str(period), "period 'period name' starting on {}".format(start))


class MembershipTests(TestCase):
    def setUp(self):
        self.period = MembershipPeriod.objects.create(short_name='2018-2019', start=timezone.now(),
                                                      end=timezone.now() + timedelta(hours=2))
        self.user = get_user_model().objects.create_user('user', 'user@provider.com', 'pwd')

    def test_empty(self):
        membership = Membership()
        self.assertTrue(membership.is_pending)
        self.assertFalse(membership.is_approved)
        self.assertFalse(membership.is_rejected)
        self.assertEqual(membership.status, "Pending approval")

    def test_approved(self):
        membership = Membership(period=self.period, user_profile=self.user.profile)

        membership.approve()

        self.assertEqual(str(membership),
                         " <user@provider.com>'s membership for the period 2018-2019 ({})".format(membership.status))

        # Check that we do not allow double approval
        self.assertRaisesMessage(ValueError, 'The membership has already been approved', membership.approve)

        self.assertFalse(membership.is_pending)
        self.assertTrue(membership.is_approved)
        self.assertFalse(membership.is_rejected)
        self.assertEqual(membership.status, "Approved on {}".format(membership.approved_on.date()))

        # Check an email has been sent
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.user.email])
        self.assertIn("approved", mail.outbox[0].subject)
        self.assertIn("approved", mail.outbox[0].body)
        self.assertIn(self.period.short_name, mail.outbox[0].body)

    def test_rejected(self):
        membership = Membership(period=self.period, user_profile=self.user.profile)

        # Check that we do not allow a rejection without a reason
        self.assertRaisesMessage(ValueError, 'A membership cannot be rejected without a reason',
                                 membership.reject, reason=None)
        self.assertRaisesMessage(ValueError, 'A membership cannot be rejected without a reason',
                                 membership.reject, reason='')

        membership.reject('No real reason')

        self.assertEqual(str(membership),
                         " <user@provider.com>'s membership for the period 2018-2019 ({})".format(membership.status))

        # Check that we do not allow double rejection
        self.assertRaisesMessage(ValueError, 'The membership has already been rejected',
                                 membership.reject, reason='No real reason')

        self.assertFalse(membership.is_pending)
        self.assertFalse(membership.is_approved)
        self.assertTrue(membership.is_rejected)
        self.assertEqual(membership.status, "Rejected on {}".format(membership.rejected_on.date()))

        # Check an email has been sent
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.user.email])
        self.assertIn("rejected", mail.outbox[0].subject)
        self.assertIn("declined", mail.outbox[0].body)
        self.assertIn(membership.rejection_reason, mail.outbox[0].body)
        self.assertIn(self.period.short_name, mail.outbox[0].body)

    def test_approved_and_rejected(self):
        membership = Membership(approved_on=timezone.now(), rejected_on=timezone.now())
        self.assertFalse(membership.is_pending)
        self.assertFalse(membership.is_approved)
        self.assertTrue(membership.is_rejected)
        self.assertEqual(membership.status, "Rejected on {}".format(membership.rejected_on.date()))

    @patch('gui.models.Membership.objects.filter')
    def test_members(self, filter_mocked):
        memberships = Membership.pending_memberships()
        filter_mocked.assert_called_with(approved_on=None, rejected_on=None)
        filter_mocked.return_value.order_by.assert_called_with('id')
        self.assertEqual(memberships, filter_mocked.return_value.order_by.return_value)

    @patch('gui.models.Membership.is_pending', new_callable=PropertyMock)
    @patch('gui.models.Membership.is_approved', new_callable=PropertyMock)
    @patch('gui.models.Membership.is_rejected', new_callable=PropertyMock)
    def test_unknown_state(self, rejected_mock, approved_mock, pending_mock):
        rejected_mock.return_value = False
        approved_mock.return_value = False
        pending_mock.return_value = False

        membership = Membership(approved_on=timezone.now(), rejected_on=timezone.now())
        with self.assertRaises(ValueError, msg="Unknown state"):
            membership.status


class ProfileTests(TestCase):
    def create_user(self, username, first_name="First", last_name="Last", email='a@x.org',
                    affiliation='', public_statement=''):
        user = get_user_model().objects.create(username=username, first_name="First",
                                               last_name="Last", email='a@x.org')
        user.profile.affiliation = affiliation
        user.public_statement = public_statement
        user.save()
        return user

    def setUp(self):
        self.user = self.create_user(username='user')
        self.profile = self.user.profile

    def test_membership__no_period(self):
        self.assertEqual(self.profile.membership, None)

    def test_membership__with_active_period_but_no_membership_application(self):
        MembershipPeriod.objects.create(start=timezone.now() - timedelta(hours=1),
                                        end=timezone.now() + timedelta(hours=2))
        self.assertEqual(self.profile.membership, None)

    def test_membership__with_active_period_and_membership_application(self):
        period = MembershipPeriod.objects.create(start=timezone.now() - timedelta(hours=1),
                                                 end=timezone.now() + timedelta(hours=2))
        membership = Membership.objects.create(period=period, user_profile=self.profile)
        self.assertEqual(self.profile.membership, membership)

    def test_last_membership__no_periods(self):
        self.assertEqual(self.profile.last_membership, None)

    def test_last_membership__with_periods(self):
        period1 = MembershipPeriod.objects.create(short_name="p1", start=timezone.now(),
                                                  end=timezone.now() + timedelta(hours=2))
        period2 = MembershipPeriod.objects.create(short_name="p2", start=timezone.now(),
                                                  end=timezone.now() + timedelta(hours=2))
        period3 = MembershipPeriod.objects.create(short_name="p3", start=timezone.now(),
                                                  end=timezone.now() + timedelta(hours=2))

        # Create 3 memberships, but the last one pending approval by administrators
        Membership.objects.create(period=period1, user_profile=self.profile, approved_on=timezone.now())
        Membership.objects.create(period=period2, user_profile=self.profile, approved_on=timezone.now())
        Membership.objects.create(period=period3, user_profile=self.profile)

        self.assertTrue(self.profile.last_membership, period2)

    @patch('gui.models.Ballot.active_ballots', return_value=[MagicMock(), MagicMock(), MagicMock()])
    def test_active_ballots(self, ballots_mocked):
        self.profile.membership = MagicMock()

        ballots = self.profile.active_ballots

        self.assertEqual(len(ballots), 3)
        for i, res in enumerate(ballots):
            ballot, has_voted = res
            self.assertEqual(ballot, ballots_mocked.return_value[i])
            ballot.has_voted.assert_called_with(self.profile.membership)
            self.assertEqual(has_voted, ballot.has_voted.return_value)

    def test_str(self):
        self.assertEqual(str(self.profile), "First Last <a@x.org>")

    def test_existing_affiliations(self):
        for i, affiliation in enumerate(['Intel', 'Samsung', 'Suse', 'Red Hat', '', 'Intel',
                                         'Canonical']):
            self.create_user(username="user{}".format(i), affiliation=affiliation)

        self.assertEqual(Profile.existing_affiliations(),
                         ['Canonical', 'Intel', 'Red Hat', 'Samsung', 'Suse'])

    def test_send_email(self):
        self.profile.send_email("My subject", "My message")

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.user.email])
        self.assertTrue("My subject" in mail.outbox[0].subject)
        self.assertTrue("My message" in mail.outbox[0].body)


class CP_AdminTasksCountTests(TestCase):
    @patch('gui.models.Membership.pending_memberships',
           return_value=MagicMock(count=MagicMock(return_value=42)))
    def test_admin_tasks_count__normal_user(self, pending_memberships_mock):
        request = MagicMock(user=MagicMock(is_superuser=False))
        self.assertEqual(admin_tasks_count(request),
                         {"admin_tasks_count": 0})

    @patch('gui.models.Membership.pending_memberships',
           return_value=MagicMock(count=MagicMock(return_value=42)))
    def test_admin_tasks_count__superuser(self, pending_memberships_mock):
        request = MagicMock(user=MagicMock(is_superuser=True))
        self.assertEqual(admin_tasks_count(request),
                         {"admin_tasks_count": 42})


class CP_global_context(TestCase):
    @patch('gui.models.MembershipPeriod.current_period',
           return_value=MagicMock())
    def test_current_period(self, current_period_mock):
        self.assertEqual(global_context(None).get("current_membership_period"),
                         current_period_mock.return_value)
        current_period_mock.assert_called_with()

    def test_website(self):
        website = global_context(None).get("website")

        with patch.dict('os.environ', {}, clear=True):
            self.assertEqual(website.version, None)

        with patch.dict('os.environ', {"WEBSITE_VERSION": "1234"}, clear=True):
            self.assertEqual(website.version, "1234")
            self.assertTrue(website.project_url.startswith("https://"))
            self.assertIn(website.project_url, website.version_url)
            self.assertIn(website.version, website.version_url)


class ProfileFormTests(TestCase):
    def test_empty_form(self):
        form = ProfileForm({})

        user = MagicMock()
        form.save(user)

        self.assertFalse(form.is_valid())
        user.save.assert_not_called()

    def test_all_but_affiliation(self):
        form = ProfileForm({'first_name': 'First', 'last_name': 'Last', 'public_statement': 'My statement'})

        user = MagicMock()
        form.save(user)

        self.assertTrue(form.is_valid())
        self.assertEqual(user.first_name, 'First')
        self.assertEqual(user.last_name, 'Last')
        self.assertEqual(user.profile.affiliation, '')
        self.assertEqual(user.profile.public_statement, 'My statement')
        user.save.assert_called_with()


class MembershipApplicationFormTests(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(first_name="First", last_name="Last", email='a@x.org')
        self.period = MembershipPeriod.objects.create(start=timezone.now(),
                                                      end=timezone.now() + timedelta(hours=2))

    @patch('gui.models.Membership.objects.create')
    def test_empty_form(self, create_mock):
        form = MembershipApplicationForm({})
        self.assertFalse(form.save())

        self.assertFalse(form.is_valid())
        create_mock.assert_not_called()

    @patch('gui.models.Membership.objects.create')
    def test_not_agreeing_to_membership(self, create_mock):
        form = MembershipApplicationForm({"period_id": self.period.id, "user_id": self.user.id,
                                          "public_statement": "My statement", "agree_membership": False})
        self.assertFalse(form.save())

        self.assertFalse(form.is_valid())
        self.assertEqual(get_user_model().objects.get(pk=self.user.id).profile.public_statement, "")
        create_mock.assert_not_called()

    @patch('gui.models.Membership.objects.create')
    def test_all_valid(self, create_mock):
        form = MembershipApplicationForm({"period_id": self.period.id, "user_id": self.user.id,
                                          "public_statement": "My statement", "agree_membership": True})
        self.assertTrue(form.save())

        self.assertTrue(form.is_valid(), form.errors)
        self.assertEqual(get_user_model().objects.get(pk=self.user.id).profile.public_statement, "My statement")
        create_mock.assert_called_with(period=self.period, user_profile=self.user.profile)


class GuiViewMixin:
    def setUpGuiTests(self, url, verified_user_needed=False, superuser_needed=False):
        self.url = url
        self.verified_user_needed = verified_user_needed or superuser_needed
        self.superuser_needed = superuser_needed

        if os.environ.get('VALIDATE_HTML') is not None:
            self.client = ValidatingClient()  # pragma: no cover

        # HACK: Massively speed up the login primitive. We don't care about security in tests
        settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )

    def create_user_and_log_in(self, admin=False, verified=False):
        return create_user_and_log_in(self.client, verified=verified, admin=admin)

    def do_POST(self, params):
        response = self.client.post(self.url, params)
        self.assertEqual(response.status_code, 302)
        return self.client.get(response.url, {})

    def test_unauthenticated_user(self):
        response = self.client.get(self.url)

        if self.verified_user_needed:
            self.assertEqual(response.status_code, 302)
        else:
            self.assertEqual(response.status_code, 200)

        # Base.html checks
        if not self.verified_user_needed:
            self.assertContains(response, ">Sign In</a>")
            self.assertContains(response, ">Sign Up</a>")
            self.assertNotContains(response, '>Admin <span class="badge">')

        self.additional_unauthenticated_checks(response)

    def test_logged_in_unverified_user(self):
        self.create_user_and_log_in(admin=False, verified=False)

        # make sure no email is getting send, which slows down tests
        with self.settings(EMAIL_BACKEND='django.core.mail.backends.dummy.EmailBackend'):
            response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

        # Base.html checks
        if not self.verified_user_needed:
            self.assertContains(response, ">Your Profile</a>")
            self.assertContains(response, ">Change E-mail</a>")
            self.assertContains(response, ">Sign Out</a>")
            self.assertNotContains(response, '>Admin <span class="badge">')

            self.additional_logged_in_user_checks(response)

        return response

    def test_logged_in_verified_user(self):
        self.create_user_and_log_in(admin=False, verified=True)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403 if self.superuser_needed else 200)

        # Base.html checks
        if not self.superuser_needed:
            self.assertContains(response, ">Your Profile</a>")
            self.assertContains(response, ">Change E-mail</a>")
            self.assertContains(response, ">Sign Out</a>")
            self.assertNotContains(response, '>Admin <span class="badge">')

        self.additional_logged_in_user_checks(response)

        return response

    @patch('gui.models.Membership.pending_memberships',
           return_value=MagicMock(count=MagicMock(return_value=42)))
    def test_logged_in_admin(self, admin_tasks_count_mocked):
        self.create_user_and_log_in(admin=True, verified=True)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

        # Base.html checks
        self.assertContains(response, ">Your Profile</a>")
        self.assertContains(response, ">Change E-mail</a>")
        self.assertContains(response, ">Sign Out</a>")
        self.assertContains(response, '>Admin <span class="badge">')
        self.assertContains(response, '>Approve memberships <span class="badge">42</span>')

        self.additional_admin_checks(response)

        return response

    # To be overriden
    def additional_unauthenticated_checks(self, response):
        pass

    def additional_logged_in_user_checks(self, response):
        pass

    def additional_admin_checks(self, response):
        pass


class ViewIndexTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('index'))

    def create_valid_member(self):
        user = self.create_user_and_log_in(verified=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))
        membership = Membership.objects.create(period=period, user_profile=user.profile,
                                               approved_on=timezone.now())
        return membership

    def test_check_links(self):
        # Create a set of links and their corresponding HTML
        links = []
        for i in range(0, 10):
            short_name = "Link_{}".format(i)
            url = "http://link-{}.com".format(i)
            Link.objects.create(short_name=short_name, index_position=10-i, url=url,
                                description="My desc")
            links.append('<a href="{}" title="My desc">{}</a>'.format(url, short_name))

        # Now check that everything is alright
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(response.context['links']),
                         list(Link.objects.all().order_by("index_position")))
        for link in links:
            self.assertContains(response, link)

    def additional_unauthenticated_checks(self, response):
        self.assertContains(response, "Log-in or Sign-up")

        self.assertNotContains(response, 'href="{}"'.format(reverse('members')))

    def additional_logged_in_user_checks(self, response):
        self.assertContains(response, 'role="button">Apply</a>')

        self.assertNotContains(response, 'href="{}"'.format(reverse('members')))

    def additional_admin_checks(self, response):
        self.additional_logged_in_user_checks(response)

    def test_user_with_pending_membership(self):
        user = self.create_user_and_log_in(verified=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))
        Membership.objects.create(period=period, user_profile=user.profile)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Thanks for applying, your application is being processed!")

        self.assertNotContains(response, 'href="{}"'.format(reverse('members')))

    def test_user_with_active_membership(self):
        self.create_valid_member()

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Your application has been approved, thanks for being a member!")

        self.assertContains(response, 'href="{}"'.format(reverse('members')))

    def test_user_with_rejected_membership(self):
        user = self.create_user_and_log_in(verified=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))
        Membership.objects.create(period=period, user_profile=user.profile,
                                  rejected_on=timezone.now(), rejection_reason="You\nare\na\ntroll")

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "<p>Your application has been rejected. Reason:</p>")
        self.assertContains(response, "<p>You<br>are<br>a<br>troll</p>")

        self.assertNotContains(response, 'href="{}"'.format(reverse('members')))

    def test_open_ballot_not_voted_yet(self):
        self.create_valid_member()
        ballot = Ballot.objects.create(short_name='2019-2020', description="Ballot description 42",
                                       opening_on=timezone.now(),
                                       closing_on=timezone.now() + timedelta(hours=1))

        # Check that the page has a link to the active ballot
        response = self.client.get(self.url)
        self.assertContains(response, "Active ballots (1)")
        self.assertContains(response, ballot.short_name)
        self.assertContains(response, ballot.description)
        self.assertContains(response, reverse('ballot-vote', kwargs={"pk": ballot.id}))

    @patch('gui.models.Ballot.has_voted', return_value=True)
    def test_open_ballot_has_already_voted(self, has_voted_mocked):
        self.create_valid_member()
        ballot = Ballot.objects.create(short_name='2019-2020', description="Ballot description 42",
                                       opening_on=timezone.now(),
                                       closing_on=timezone.now() + timedelta(hours=1))

        # Check that the page has a link to the active ballot
        response = self.client.get(self.url)
        self.assertContains(response, "Active ballots (1)")
        self.assertContains(response, ballot.short_name)
        self.assertContains(response, ballot.description)
        self.assertContains(response, " (voted)")
        self.assertNotContains(response, reverse('ballot-vote', kwargs={"pk": ballot.id}))

    def test_closed_ballot(self):
        self.create_valid_member()
        ballot = Ballot.objects.create(short_name='2019-2020', description="Ballot description 42",
                                       opening_on=timezone.now(), closing_on=timezone.now())

        # Check that the page has a link to the active ballot
        response = self.client.get(self.url)
        self.assertNotContains(response, "Active ballots")
        self.assertNotContains(response, ballot.short_name)
        self.assertNotContains(response, ballot.description)
        self.assertNotContains(response, reverse('ballot-vote', kwargs={"pk": ballot.id}))


class ViewMembersTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('members'), verified_user_needed=True)

        self.period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                      end=timezone.now() + timedelta(hours=2))

    def _create_membership(self, admin=False, verified=True):
        self.user = create_user_and_log_in(self.client, verified=verified, admin=admin)
        self.membership = Membership.objects.create(period=self.period, user_profile=self.user.profile)
        return self.membership

    def create_user_and_log_in(self, admin=False, verified=True):
        self._create_membership(admin=admin, verified=verified).approve()
        return self.user

    def test_membership_pending(self):
        self._create_membership()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_rejected_member(self):
        self._create_membership().reject("No reason")
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_proper_member(self):
        self._create_membership().approve()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    @patch('gui.models.MembershipPeriod.members')
    def test_no_members(self, members_mock):
        response = self.test_logged_in_verified_user()

        # Check that the members list has been generated with the right calls
        members_mock.order_by.assert_called_with("user_profile__user__last_name")
        members_mock.order_by.return_value.select_related.assert_called_with('user_profile',
                                                                             'user_profile__user')
        self.assertEqual(response.context['object_list'],
                         members_mock.order_by.return_value.select_related.return_value)

        self.assertContains(response, "No members yet.")

    def __setup_db(self):
        for i in range(0, 3):
            username = "user_{}".format(i)
            user = get_user_model().objects.create(first_name=username, last_name="last",
                                                   username=username)
            user.is_superuser = (i == 1)
            user.profile.affiliation = "" if i == 0 else "Intel"
            user.profile.public_statement = "My public\nstatement"
            user.save()
            Membership.objects.create(period=self.period, user_profile=user.profile, approved_on=timezone.now())

    def test_with_members_as_member(self):
        self.__setup_db()

        response = self.test_logged_in_verified_user()
        self.assertContains(response, "<td>user_0 last</td>")
        self.assertContains(response, "<td>user_1 last</td>")
        self.assertContains(response, "<td>user_2 last</td>")
        self.assertContains(response, "<td>Unknown</td>")
        self.assertContains(response, "<td>Intel</td>")
        self.assertContains(response, "<td><p>My public<br>statement</p></td>")

        # Verify that the administrator-only actions are not visible
        self.assertNotContains(response, "<th>Action</th>")
        self.assertNotContains(response, 'value="Make Admin"')
        self.assertNotContains(response, 'value="Make User"')
        self.assertNotContains(response, "No actions available")

    def test_with_members_as_admin(self):
        self.__setup_db()

        # Verify that the administrator-only actions are visible
        response = self.test_logged_in_admin()
        self.assertContains(response, "<th>Action</th>")
        self.assertContains(response, 'value="Make Admin"')
        self.assertContains(response, 'value="Make User"')
        self.assertContains(response, '<form action="/members/1/make/admin" method="post">')
        self.assertContains(response, '<form action="/members/2/make/user" method="post">')
        self.assertContains(response, '<form action="/members/3/make/admin" method="post">')

        self.assertContains(response, "No actions available")
        self.assertNotContains(response, '<form action="/members/{}/make/user" method="post">'.format(self.user.id))


class ViewChangeUserStatus(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user('not_admin', 'user@provider.com', 'pwd',
                                                         is_superuser=False)

    def test_get_and_admin(self):
        create_user_and_log_in(self.client, admin=True, verified=True)
        response = self.client.get(reverse('members-user-to-admin', kwargs={"pk": self.user.id}))
        self.assertEqual(response.status_code, 405)

    def test_non_admin(self):
        create_user_and_log_in(self.client, admin=False, verified=True)
        response = self.client.post(reverse('members-user-to-admin', kwargs={"pk": self.user.id}))
        self.assertEqual(response.status_code, 403)

    def test_admin_post_with_referrer(self):
        admin = create_user_and_log_in(self.client, admin=True, verified=True)
        referrer = 'http://my.website.com/foo/bar'

        self.assertFalse(get_user_model().objects.get(pk=self.user.id).is_superuser)
        self.assertFalse(get_user_model().objects.get(pk=self.user.id).is_staff)
        response = self.client.post(reverse('members-user-to-admin', kwargs={"pk": self.user.id}),
                                    {}, HTTP_REFERER=referrer)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, referrer)
        self.assertTrue(get_user_model().objects.get(pk=self.user.id).is_superuser)
        self.assertTrue(get_user_model().objects.get(pk=self.user.id).is_staff)

        # Check an email has been sent
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.user.email])
        self.assertIn("Your status changed from 'user' to 'administrator'", mail.outbox[0].subject)
        self.assertIn("'user' to 'administrator'", mail.outbox[0].body)
        self.assertIn(admin.get_full_name(), mail.outbox[0].body)

        response = self.client.post(reverse('members-admin-to-user', kwargs={"pk": self.user.id}),
                                    {}, HTTP_REFERER=referrer)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, referrer)
        self.assertFalse(get_user_model().objects.get(pk=self.user.id).is_superuser)

        # Check an email has been sent
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].to, [self.user.email])
        self.assertIn("Your status changed from 'administrator' to 'user'", mail.outbox[1].subject)
        self.assertIn("'administrator' to 'user'", mail.outbox[1].body)
        self.assertIn(admin.get_full_name(), mail.outbox[1].body)

    def test_admin_change_its_own_status(self):
        user = create_user_and_log_in(self.client, admin=True, verified=True)
        response = self.client.post(reverse('members-admin-to-user', kwargs={"pk": user.id}))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(get_user_model().objects.get(pk=user.id).is_superuser)


class ViewProfileTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('account-profile'), verified_user_needed=True)

    def test_post_invalid_form(self):
        self.create_user_and_log_in(verified=True)
        response = self.do_POST({})
        self.assertContains(response, "Your profile is invalid")

    def test_post_valid_form(self):
        self.create_user_and_log_in(verified=True)
        response = self.do_POST({"first_name": "First",
                                 "last_name": "Last",
                                 "public_statement": "Statement"})
        self.assertContains(response, "Your profile was updated successfully")

    @patch('gui.models.Profile.existing_affiliations', return_value=['Collabora', 'Intel',
                                                                     'Google', 'Samsung'])
    def test_existing_affiliations_list(self, existing_affiliations_mock):
        self.create_user_and_log_in(verified=True)
        response = self.client.get(self.url)

        self.assertContains(response, '<datalist id="list__affiliations-list"><option value="Collabora">'
                            '<option value="Intel"><option value="Google"><option value="Samsung"></datalist>')


class ViewDeleteAccountTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('account-delete'), verified_user_needed=True)

    def test_do_delete(self):
        self.create_user_and_log_in(verified=True)
        response = self.do_POST({})
        self.assertContains(response, 'Your account has been deleted successfully')


class ViewMembershipApplicationTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('membership-application'), verified_user_needed=True)

    def test_no_periods(self):
        response = self.test_logged_in_admin()
        self.assertContains(response, "No membership period has been created.")

    def test_with_period(self):
        MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                        end=timezone.now() + timedelta(hours=2))

        response = self.test_logged_in_admin()
        self.assertContains(response, "Membership application - 2018-2019")

    def test_user_with_current_membership(self):
        user = self.create_user_and_log_in(verified=True, admin=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))
        Membership.objects.create(period=period, user_profile=user.profile)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, "You already applied to the period 2018-2019. Current status: Pending approval")

    def test_post_invalid_form(self):
        self.create_user_and_log_in(verified=True, admin=True)
        MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                        end=timezone.now() + timedelta(hours=2))
        response = self.do_POST({})
        self.assertContains(response, "Your application is invalid")

    def test_post_invalid_form_no_agreement(self):
        user = self.create_user_and_log_in(verified=True, admin=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))

        response = self.do_POST({"period_id": period.id,
                                 "user_id": user.id,
                                 "public_statement": "My statement",
                                 "agree_membership": False})
        self.assertContains(response, "Your application is invalid")

    def test_post_valid_form(self):
        user = self.create_user_and_log_in(verified=True, admin=True)
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))

        response = self.do_POST({"period_id": period.id,
                                 "user_id": user.id,
                                 "public_statement": "My statement",
                                 "agree_membership": True})
        self.assertContains(response, "Your application was sent successfully and will be reviewed shortly")

        # Check that an email has been sent to the admins
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn(user.email, mail.outbox[0].to)
        self.assertIn(reverse('membership-approval'), mail.outbox[0].body)


class MembershipPeriodCreateViewTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('membership-period-create'), superuser_needed=True)

    @patch('gui.models.MembershipPeriod.current_period',
           return_value=MagicMock(short_name="2042-2043", description="Period description",
                                  agreement_url="https://x.org/agreement.pdf",
                                  start=timezone.now(),
                                  end=timezone.now() + timedelta(hours=2)))
    def test_default_values(self, period_mocked):
        self.create_user_and_log_in(verified=True, admin=True)
        response = self.client.get(self.url, {})
        self.assertContains(response, period_mocked.return_value.short_name)
        self.assertContains(response, period_mocked.return_value.description)
        self.assertContains(response, period_mocked.return_value.agreement_url)
        self.assertContains(response, period_mocked.return_value.start.strftime("%Y-%m-%d %H:%M:%S"))

    def test_post_invalid_form(self):
        self.create_user_and_log_in(verified=True, admin=True)
        response = self.client.post(self.url, {})
        self.assertEqual(response.request.get('PATH_INFO'), self.url)
        self.assertContains(response, "has-error")

    def test_post_valid_form(self):
        self.create_user_and_log_in(verified=True, admin=True)
        response = self.client.post(self.url, {"short_name": "My period",
                                               'description': "My description",
                                               "agreement_url": "https://x.org",
                                               "start": '2018-01-01',
                                               "end": '2019-01-01'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('index'))


class ViewMembershipApprovalTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('membership-approval'), superuser_needed=True)

    def test_post_non_integer_ids(self):
        self.create_user_and_log_in(admin=True, verified=True)
        response = self.do_POST({'ids': ["0", "1", "2", "a", "d"]})
        self.assertContains(response, "ERROR: One or more IDs are not integers...")

    def test_post_invalid_action(self):
        self.create_user_and_log_in(admin=True, verified=True)
        response = self.do_POST({'ids': [], "action": "invalid"})
        self.assertContains(response, "ERROR: Unsupported action...")

    def test_post_approve_action(self):
        user = self.create_user_and_log_in(admin=True, verified=True)
        period1 = MembershipPeriod.objects.create(short_name="2017-2018", start=timezone.now(),
                                                  end=timezone.now() + timedelta(hours=2))
        period2 = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                  end=timezone.now() + timedelta(hours=2))
        membership1 = Membership.objects.create(period=period1, user_profile=user.profile)
        membership2 = Membership.objects.create(period=period2, user_profile=user.profile)

        response = self.do_POST({'ids': [membership1.id, membership2.id], "action": "approve"})
        self.assertContains(response, "You approved 2 membership applications")

    def test_post_reject_action_no_reasons(self):
        user = self.create_user_and_log_in(admin=True, verified=True)
        period1 = MembershipPeriod.objects.create(short_name="2017-2018", start=timezone.now(),
                                                  end=timezone.now() + timedelta(hours=2))
        period2 = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                  end=timezone.now() + timedelta(hours=2))
        membership1 = Membership.objects.create(period=period1, user_profile=user.profile)
        membership2 = Membership.objects.create(period=period2, user_profile=user.profile)

        response = self.do_POST({'ids': [membership1.id, membership2.id], "action": "reject"})
        expected = "Can&#x27;t refuse First Last &lt;user@provider.com&gt;&#x27;s application without a reason"
        self.assertContains(response, expected)

    def test_post_reject_action_with_reasons(self):
        user = self.create_user_and_log_in(admin=True, verified=True)
        period1 = MembershipPeriod.objects.create(short_name="2017-2018", start=timezone.now(),
                                                  end=timezone.now() + timedelta(hours=2))
        period2 = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                  end=timezone.now() + timedelta(hours=2))
        membership1 = Membership.objects.create(period=period1, user_profile=user.profile)
        membership2 = Membership.objects.create(period=period2, user_profile=user.profile)

        response = self.do_POST({'ids': [membership1.id, membership2.id],
                                 'reject_reason_{}'.format(membership1.id): "Reason 1",
                                 'reject_reason_{}'.format(membership2.id): "Reason 2",
                                 "action": "reject"})
        self.assertContains(response, "You rejected 2 membership applications")

    def test_with_many_applications(self):
        # Create a ton of membership to check that the pagination is working
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))
        for i in range(30):
            user = get_user_model().objects.create_user('user_{}'.format(i))
            Membership.objects.create(period=period, user_profile=user.profile)

        self.create_user_and_log_in(admin=True, verified=True)

        response = self.client.get(self.url)
        self.assertNotContains(response, 'name="ids" value="11"')

        response = self.client.get(self.url + "?page=2")
        self.assertContains(response, 'name="ids" value="11"')

        response = self.client.get(self.url + "?page=3")
        self.assertNotContains(response, 'name="ids" value="11"')


class ViewAboutTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('about'))


class BallotTests(TestCase):
    fixtures = ['ballots.yaml']

    @patch('gui.models.Ballot.objects.filter')
    def test_active_ballots(self, filter_mocked):
        Ballot.active_ballots()

        args, kwargs = filter_mocked.call_args_list[0]
        self.assertLess(timezone.now() - kwargs['opening_on__lte'], timedelta(seconds=.1))
        self.assertLess(timezone.now() - kwargs['closing_on__gt'], timedelta(seconds=.1))

    def test_potential_voters(self):
        ballot = Ballot.objects.get(id=1)
        potential_voters = set()
        for i in range(1, 5):
            potential_voters.add(Membership.objects.get(id=i))
        self.assertEqual(ballot.potential_voters, potential_voters)

    def test_missing_voters(self):
        ballot = Ballot.objects.get(id=1)
        missing_voter = Membership.objects.get(id=4)
        self.assertEqual(ballot.missing_voters, set([missing_voter]))

    @patch('gui.models.MembershipPeriod.active_period_at', return_value=None)
    def test_missing_voters__without_membershipPeriod(self, cur_period_mock):
        ballot = Ballot.objects.get(id=1)
        self.assertEqual(ballot.missing_voters, set())

    def test_time_helpers(self):
        now = timezone.now()

        ballot_early = Ballot(opening_on=now+timedelta(hours=.5), closing_on=now+timedelta(hours=1))
        self.assertFalse(ballot_early.is_active)
        self.assertFalse(ballot_early.has_started)
        self.assertFalse(ballot_early.has_closed)

        ballot_active = Ballot(opening_on=now, closing_on=now+timedelta(hours=1))
        self.assertTrue(ballot_active.is_active)
        self.assertTrue(ballot_active.has_started)
        self.assertFalse(ballot_active.has_closed)

        ballot_over = Ballot(opening_on=now, closing_on=now)
        self.assertFalse(ballot_over.is_active)
        self.assertTrue(ballot_over.has_started)
        self.assertTrue(ballot_over.has_closed)

    @patch('gui.models.Ballot.voters')
    def test_has_voted(self, filter_mocked):
        membership = MagicMock()
        Ballot().has_voted(membership)

        filter_mocked.filter.assert_called_with(pk=membership.pk)
        filter_mocked.filter.return_value.exists.assert_called_with()

    def test_turnout(self):
        ballot = Ballot.objects.get(id=1)
        self.assertEqual(ballot.turnout, VoteCount(3, 4))

    def test_turnout_with_no_active_period(self):
        MembershipPeriod.current_period().delete()
        ballot = Ballot.objects.get(id=1)
        self.assertEqual(ballot.turnout, VoteCount(0, 0))

    def test_turnout_after_new_period_started(self):
        MembershipPeriod.objects.create(short_name="new period", start=timezone.now(),
                                        end=timezone.now() + timedelta(hours=1))
        ballot = Ballot.objects.get(id=1)
        self.assertEqual(ballot.turnout, VoteCount(3, 4))

    def test_send_reminder(self):
        ballot = Ballot.objects.get(id=1)

        ballot.send_reminder()

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, ['slacker@example.com'])

        self.assertIn("vote", mail.outbox[0].subject)
        self.assertIn('2019-2020', mail.outbox[0].body)

        self.assertIn("vote", mail.outbox[0].body)
        self.assertIn('2019-2020', mail.outbox[0].body)
        self.assertIn(reverse('ballot-vote', kwargs={"pk": ballot.id}), mail.outbox[0].body)

    def test_str(self):
        self.assertEqual(str(Ballot.objects.get(id=1)), "Ballot<2019-2020>")


class BallotFormTests(TestCase):
    def setUp(self):
        self.ballot = Ballot.objects.create(short_name='2019-2020', opening_on=timezone.now(),
                                            closing_on=timezone.now())

        # Create select questions
        self.select_answers = OrderedDict()
        for i in range(3):
            question = BallotSelectQuestion.objects.create(ballot=self.ballot,
                                                           question='Select question #{}'.format(i))
            self.select_answers[question] = []
            for a in range(3):
                sqpa = BallotSelectQuestionPossibleAnswer.objects.create(question=question,
                                                                         answer="question {} answer {}".format(i, a))
                self.select_answers[question].append(sqpa)

        # Create ranking questions
        self.ranking_options = OrderedDict()
        for i in range(3):
            question = BallotRankingQuestion.objects.create(ballot=self.ballot,
                                                            question='Ranking question #{}'.format(i))
            self.ranking_options[question] = []
            for o in range(3):
                option = BallotRankingQuestionOption.objects.create(question=question,
                                                                    option="ranking {} option {}".format(i, o))
                self.ranking_options[question].append(option)

    def test_ballot_not_specified(self):
        form = BallotForm()

        # Without a ballot, we should not have any fields
        self.assertEqual(sorted(form.fields.keys()), [])

    def test_dynamic_form_generation(self):
        form = BallotForm(ballot=self.ballot, data={})

        # Check that all the expected fields are here
        self.assertEqual(sorted(form.fields.keys()),
                         ['ranking_0', 'ranking_1', 'ranking_2', 'select_0', 'select_1', 'select_2'])

        # Check that we created the select fields correctly
        for i in range(3):
            field_name = 'select_{}'.format(i)
            self.assertTrue(form.fields[field_name].required)
            self.assertEqual(form.fields[field_name].choices,
                             [('', '-- Select an answer --'),
                              (i * 3 + 1, 'question {} answer 0'.format(i)),
                              (i * 3 + 2, 'question {} answer 1'.format(i)),
                              (i * 3 + 3, 'question {} answer 2'.format(i))])
            self.assertEqual(form.fields[field_name].label, "Select question #{}".format(i))

        # Check that we created the ranking fields correctly
        for i in range(3):
            field_name = 'ranking_{}'.format(i)
            for o in range(3):
                ranking_field = form.fields[field_name].fields[o]

                # Check the field
                self.assertEqual(ranking_field.choices,
                                 [(i * 3 + 1, 'ranking {} option 0'.format(i)),
                                  (i * 3 + 2, 'ranking {} option 1'.format(i)),
                                  (i * 3 + 3, 'ranking {} option 2'.format(i)),
                                  (-1, '-- Abstain --')])
                self.assertEqual(ranking_field.label, str(o))

                # Check the associated widget
                widget = form.fields[field_name].widget.widgets[o]
                self.assertEqual(widget.get_context("", "", [])['widget'].get('rank'), o + 1)
                self.assertEqual(widget.choices,
                                 [('', '-- Select an option --')] + ranking_field.choices)
                self.assertEqual(widget.rank, o + 1)

    def test_OptionRankingWidget_decompress(self):
        form = BallotForm(ballot=self.ballot, data={})
        widget = form.fields["ranking_0"].widget
        self.assertEqual(widget.decompress(None), [None, None, None])
        self.assertEqual(widget.decompress(['toto', 'tata']), ['toto', 'tata'])

    def test_empty_form(self):
        form = BallotForm(ballot=self.ballot, data={})

        # Check that the form is invalid and that the save method fails too
        self.assertFalse(form.is_valid())
        self.assertFalse(form.save(MagicMock()))

    def test_get_objects__non_integer_ids(self):
        form = BallotForm(ballot=self.ballot, data={})
        form._get_objects("ranking_0", Ballot, ['1', 'toto', '2'])
        self.assertEqual(form.errors.get("ranking_0"), ['This field is required.',
                                                        'The selected ids are not integer'])

    def test_get_objects__duplicated_ids(self):
        form = BallotForm(ballot=self.ballot, data={})
        form._get_objects("ranking_0", Ballot, ['1', '1', '2'])
        self.assertEqual(form.errors.get("ranking_0"), ['This field is required.',
                                                        'Options cannot be duplicated'])

    def test_get_objects__multiple_abstain_ids_between_options(self):
        form = BallotForm(ballot=self.ballot, data={})
        form._get_objects("ranking_0", Ballot, ['1', '-1', '-1', 2])
        self.assertEqual(form.errors.get("ranking_0"), ['This field is required.',
                                                        'Only the lowest-ranking options can be abstained'])

    def test_get_objects__non_existing_ids(self):
        form = BallotForm(ballot=self.ballot, data={})
        form._get_objects("ranking_0", Ballot, ['1', '2'])
        self.assertEqual(form.errors.get("ranking_0"), ['This field is required.',
                                                        'At least one selected item is invalid'])

    def test_valid_form(self):
        form = BallotForm(ballot=self.ballot, data={
            'select_0': '1', 'select_1': '5', 'select_2': '9',
            'ranking_0_0': '1', 'ranking_0_1': '2', 'ranking_0_2': '-1',
            'ranking_1_0': '6', 'ranking_1_1': '5', 'ranking_1_2': '4',
            'ranking_2_0': '8', 'ranking_2_1': '7', 'ranking_2_2': '9',
        })

        # Check that the form is valid and that the save method fails too
        self.assertEqual(form.errors, {})
        self.assertTrue(form.is_valid())

        # Try saving the form now
        user = get_user_model().objects.create_user('user', 'user@provider.com', 'pwd')
        period = MembershipPeriod.objects.create(short_name='2018-2019', start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))
        membership = Membership.objects.create(period=period, user_profile=user.profile)
        self.assertTrue(form.save(membership))

        # Check that the membership has been added to the list of voters
        self.assertEqual(list(self.ballot.voters.all()), [membership])

        # Check that the answers created are the ones we wanted
        for i in range(3):
            question = list(self.select_answers.keys())[i]
            answer = BallotSelectedAnswer.objects.get(member=membership, question=question)
            self.assertEqual(answer.answer, self.select_answers[question][i])

        # Check that the ranking created is the one we wanted
        expected_rank = {
            0: [1, 2, 3],
            1: [3, 2, 1],
            2: [2, 1, 3]
        }
        for q, question in enumerate(self.ranking_options.keys()):
            for o, option in enumerate(self.ranking_options[question]):
                # Convert the abstain vote to option 'None'
                if q == 0 and o == 2:
                    option = None

                answer = BallotRankingAnswer.objects.get(member=membership, question=question, option=option)
                self.assertEqual(answer.rank, expected_rank[q][o])


class VoteCountTests(TestCase):
    def test_zero_votes(self):
        vote = VoteCount(0, 0)
        self.assertEqual(vote.percentage, 0)
        self.assertEqual(repr(vote), "VoteCount(0, 0)")
        self.assertEqual(str(vote), "0.0% (0 / 0)")

    def test_count_greater_than_totals(self):
        self.assertRaisesMessage(ValueError, 'The count of votes cannot be greater than the total amount of votes',
                                 VoteCount, 2, 1)

    def test_normal_case(self):
        vote = VoteCount(10, 20)
        self.assertEqual(vote.percentage, 50.0)
        self.assertEqual(repr(vote), "VoteCount(10, 20)")
        self.assertEqual(str(vote), "50.0% (10 / 20)")

    def test_equality(self):
        self.assertEqual(VoteCount(10, 20), VoteCount(10, 20))
        self.assertNotEqual(VoteCount(11, 20), VoteCount(10, 20))


class BallotSelectQuestionTests(TestCase):
    fixtures = ['ballots.yaml']

    def setUp(self):
        self.question = BallotSelectQuestion.objects.get(id=1)

    def test_tally(self):
        answer_yes = BallotSelectQuestionPossibleAnswer.objects.get(id=1)
        answer_no = BallotSelectQuestionPossibleAnswer.objects.get(id=2)

        results = self.question.tally
        self.assertEqual(list(results.keys()), [answer_yes, answer_no])
        self.assertEqual(results[answer_yes], VoteCount(2, 3))
        self.assertEqual(results[answer_no], VoteCount(1, 3))

    def test_tally_on_open_ballot(self):
        self.question.ballot.closing_on = timezone.now() + timedelta(hours=1)
        with self.assertRaises(ValueError, msg="No tally can be performed until the ballot has closed"):
            self.question.tally

    def test_str(self):
        self.assertEqual(str(self.question),
                         "Ballot<2019-2020>: Do you accept the new bylaws?")


class BallotSelectQuestionPossibleAnswerTests(TestCase):
    fixtures = ['ballots.yaml']

    def test_str(self):
        self.assertEqual(str(BallotSelectQuestionPossibleAnswer.objects.get(id=1)),
                         "Ballot<2019-2020>: Do you accept the new bylaws?: yes")


class BallotSelectedAnswerTests(TestCase):
    fixtures = ['ballots.yaml']

    def test_str(self):
        self.assertEqual(str(BallotSelectedAnswer.objects.get(id=1)),
                         "Ballot<2019-2020>: Do you accept the new bylaws? - 'first.last@example.com' selected 'no'")


class OptionRankingVotesTests(TestCase):
    fixtures = ['ballots.yaml']

    def test_normal_case(self):
        option = BallotRankingQuestionOption.objects.get(id=1)
        votes = OptionRankingVotes(option, 4)

        self.assertEqual(votes.count_by_rank, [2, 1, 0, 0])
        self.assertEqual(votes.score, 11)
        self.assertEqual(str(votes),
                         "Ballot<2019-2020>: Rank the following candidates for the X.Org 2019-2021 mandate: " +
                         "Candidate 1 - score=11")


class BallotRankingQuestionTests(TestCase):
    fixtures = ['ballots.yaml']

    def setUp(self):
        self.question = BallotRankingQuestion.objects.get(id=1)

    def test_tally(self):
        tally = self.question.tally

        # Check that the scores are right
        self.assertEqual([t.score for t in tally], [11, 8, 6, 4])

        # Check that the candidates are in the right order
        candidate1 = BallotRankingQuestionOption.objects.get(id=1)
        candidate2 = BallotRankingQuestionOption.objects.get(id=2)
        candidate3 = BallotRankingQuestionOption.objects.get(id=3)
        candidate4 = BallotRankingQuestionOption.objects.get(id=4)
        self.assertEqual([t.ranking_option for t in tally], [candidate1, candidate3, candidate2, candidate4])

    def test_tally_on_open_ballot(self):
        self.question.ballot.closing_on = timezone.now() + timedelta(hours=1)
        with self.assertRaises(ValueError, msg="No tally can be performed until the ballot has closed"):
            self.question.tally

    def test_str(self):
        self.assertEqual(str(self.question),
                         "Ballot<2019-2020>: Rank the following candidates for the X.Org 2019-2021 mandate")


class BallotRankingQuestionOptionTests(TestCase):
    fixtures = ['ballots.yaml']

    def test_str(self):
        self.assertEqual(str(BallotRankingQuestionOption.objects.get(id=1)),
                         "Ballot<2019-2020>: Rank the following candidates for the X.Org 2019-2021 mandate: " +
                         "Candidate 1")


class BallotRankingAnswerTests(TestCase):
    fixtures = ['ballots.yaml']

    def test_str(self):
        self.assertEqual(str(BallotRankingAnswer.objects.get(id=1)),
                         "Ballot<2019-2020>: Rank the following candidates for the X.Org 2019-2021 mandate: " +
                         "'first.last@example.com' - 'Candidate 1' was ranked 1st")

        self.assertEqual(str(BallotRankingAnswer.objects.get(id=12)),
                         "Ballot<2019-2020>: Rank the following candidates for the X.Org 2019-2021 mandate: " +
                         "'first.last3@example.com' - abstained for 4th rank")


class ViewBallotListTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.setUpGuiTests(reverse('ballot-list'), superuser_needed=True)

    def test_with_ballots(self):
        for i in range(3):
            Ballot.objects.create(short_name="Ballot {}".format(i), opening_on=timezone.now(),
                                  closing_on=timezone.now())
        super().test_logged_in_admin()


class ViewVoteTests(TestCase, GuiViewMixin):
    def setUp(self):
        self.ballot = Ballot.objects.create(short_name='2019-2020', opening_on=timezone.now(),
                                            closing_on=timezone.now() + timedelta(hours=1))

        self.setUpGuiTests(reverse('ballot-vote', kwargs={"pk": self.ballot.id}), verified_user_needed=True)

    def _create_membership(self, admin=False, verified=True):
        period = MembershipPeriod.objects.create(short_name="2018-2019", start=timezone.now(),
                                                 end=timezone.now() + timedelta(hours=2))
        self.user = create_user_and_log_in(self.client, verified=verified, admin=admin)
        self.membership = Membership.objects.create(period=period, user_profile=self.user.profile)
        return self.membership

    def create_user_and_log_in(self, admin=False, verified=True):
        self._create_membership(admin=admin, verified=verified).approve()
        return self.user

    def test_membership_pending(self):
        self._create_membership()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_rejected_member(self):
        self._create_membership().reject("No reason")
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_proper_member(self):
        self._create_membership().approve()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_upcoming_ballot(self):
        self._create_membership().approve()
        self.ballot.opening_on = timezone.now() + timedelta(hours=1)
        self.ballot.save()

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 404)

    def test_expired_ballot(self):
        self._create_membership().approve()
        self.ballot.closing_on = timezone.now() - timedelta(hours=1)
        self.ballot.save()

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 404)

    def test_post_valid_form(self):
        self._create_membership().approve()

        response = self.do_POST({})
        self.assertContains(response, "Thanks for casting a vote!")

        # Try voting again and check it is failing
        response = self.client.post(self.url, {})
        self.assertEqual(response.status_code, 404)


class ViewBallotAdminTests(TestCase, GuiViewMixin):
    fixtures = ['ballots.yaml']

    def setUp(self):
        self.ballot = Ballot.objects.get(pk=1)
        self.setUpGuiTests(reverse('ballot-admin', kwargs={"pk": self.ballot.id}), superuser_needed=True)

    def test_ballot_still_open(self):
        create_user_and_log_in(self.client, verified=True, admin=True)
        self.ballot.closing_on = timezone.now() + timedelta(hours=1)
        self.ballot.save()

        response = self.client.get(self.url)
        self.assertNotContains(response, "Select: ")
        self.assertNotContains(response, "Ranking: ")
        self.assertContains(response, "Send reminder")
        self.assertContains(response, "The ballot is not yet closed, and thus results cannot be presented yet.")

    def test_ballot_not_yet_open(self):
        create_user_and_log_in(self.client, verified=True, admin=True)
        self.ballot.opening_on = timezone.now() + timedelta(hours=1)
        self.ballot.closing_on = timezone.now() + timedelta(hours=2)
        self.ballot.save()

        response = self.client.get(self.url)
        self.assertNotContains(response, "Select: ")
        self.assertNotContains(response, "Ranking: ")
        self.assertNotContains(response, "Send reminder")
        self.assertContains(response, "The ballot is not yet closed, and thus results cannot be presented yet.")

    def test_ballot_is_closed(self):
        create_user_and_log_in(self.client, verified=True, admin=True)
        self.ballot.closing_on = timezone.now()
        self.ballot.save()

        response = self.client.get(self.url)
        self.assertContains(response, "Select: ")
        self.assertContains(response, "Ranking: ")
        self.assertNotContains(response, "Send reminder")
        self.assertNotContains(response, "The ballot is not yet closed, and thus results cannot be presented yet.")


class ViewBallotSendReminderTests(TestCase):
    fixtures = ['ballots.yaml']

    def setUp(self):
        self.url = reverse('ballot-send-reminder', kwargs={"pk": 1})

    def test_get_and_admin(self):
        create_user_and_log_in(self.client, admin=True, verified=True)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 405)

    def test_valid_post(self):
        create_user_and_log_in(self.client, admin=True, verified=True)

        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/")

        response = self.client.get(response.url, {})
        self.assertContains(response, "An email has been sent to all members who have not voted yet")
