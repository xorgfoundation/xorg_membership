from django.contrib import admin
from .models import Link, Profile, MembershipPeriod, Membership, Ballot
from .models import BallotSelectQuestion, BallotSelectQuestionPossibleAnswer
from .models import BallotRankingQuestion, BallotRankingQuestionOption


class LinkModel(admin.ModelAdmin):
    list_display = ('short_name', )
    search_fields = ['short_name', 'description']


admin.site.register(Link, LinkModel)


class ProfileModel(admin.ModelAdmin):
    list_display = ('user', )
    search_fields = ['user', 'affiliation']


admin.site.register(Profile, ProfileModel)


class MembershipPeriodModel(admin.ModelAdmin):
    list_display = ('short_name', 'start')
    search_fields = ['short_name', 'description']


admin.site.register(MembershipPeriod, MembershipPeriodModel)


class MembershipModel(admin.ModelAdmin):
    list_display = ('user_profile', 'period', 'applied_on', 'approved_on', 'rejected_on')
    search_fields = ['user_profile__user__first_name', 'user_profile__user__last_name',
                     'period__short_name', 'rejection_reason']


admin.site.register(Membership, MembershipModel)


class BallotModel(admin.ModelAdmin):
    list_display = ('short_name', 'created_on', 'opening_on', 'closing_on')
    search_fields = ['short_name', 'description', 'created_on', 'opening_on', 'closing_on']
    exclude = ('voters',)


admin.site.register(Ballot, BallotModel)


class BallotSelectQuestionPossibleAnswerModelInline(admin.TabularInline):
    model = BallotSelectQuestionPossibleAnswer


class BallotSelectQuestionModel(admin.ModelAdmin):
    list_display = ('ballot', 'question')
    search_fields = ['ballot__short_name', 'question']
    inlines = [BallotSelectQuestionPossibleAnswerModelInline]


admin.site.register(BallotSelectQuestion, BallotSelectQuestionModel)


class BallotRankingQuestionOptionModelInline(admin.TabularInline):
    model = BallotRankingQuestionOption


class BallotRankingQuestionModel(admin.ModelAdmin):
    list_display = ('ballot', 'question')
    search_fields = ['ballot__short_name', 'question']
    inlines = [BallotRankingQuestionOptionModelInline]


admin.site.register(BallotRankingQuestion, BallotRankingQuestionModel)
