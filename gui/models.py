from django.contrib.auth.models import User
from django.contrib.humanize.templatetags.humanize import ordinal
from django.db import models
from django.db.models import Count
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils.functional import cached_property
from django.utils import timezone

from .email import Email, email_context

from collections import OrderedDict


class Link(models.Model):
    short_name = models.CharField(max_length=100,
                                  help_text="Short name to describe the link")
    description = models.CharField(max_length=255, blank=True, null=True,
                                   help_text="Longer description")
    index_position = models.IntegerField(help_text="Smaller index means it will "
                                                   "be higher in the list",
                                         default=0)
    url = models.URLField(help_text="Path to the document/website you want to link")


class MembershipPeriod(models.Model):
    short_name = models.CharField(max_length=50, unique=True,
                                  help_text="Name/title of the period. Eg. '2017-2018'")

    description = models.TextField(help_text="Description of what the period represents. Will be "
                                             "displayed when prompting users to re-new their membership")

    agreement_url = models.URLField(help_text="URL to the membership agreement")

    start = models.DateTimeField(help_text="Date at which the new membership period starts.")

    end = models.DateTimeField(help_text=("Date at which members who did not renew their"
                                          "membership will stop receiving emails"))

    @classmethod
    def active_period_at(cls, datetime):
        return MembershipPeriod.objects.filter(start__lt=datetime, end__gt=datetime).first()

    @classmethod
    def current_period(cls):
        return MembershipPeriod.objects.exclude(start__gt=timezone.now()).order_by("start").last()

    @property
    def members(self):
        return Membership.objects.filter(period=self).exclude(approved_on=None, rejected_on=None)

    def __str__(self):
        return "period '{}' starting on {}".format(self.short_name, self.start)


class Membership(models.Model):
    period = models.ForeignKey(MembershipPeriod, on_delete=models.CASCADE)
    user_profile = models.ForeignKey("Profile", on_delete=models.CASCADE)

    applied_on = models.DateTimeField(auto_now_add=True)
    approved_on = models.DateTimeField(null=True, blank=True, db_index=True)
    rejected_on = models.DateTimeField(null=True, blank=True, db_index=True)
    rejection_reason = models.TextField(null=True, blank=True, help_text="Reason for the rejection")

    class Meta:
        unique_together = ('period', 'user_profile')

    @classmethod
    def pending_memberships(self):
        return Membership.objects.filter(approved_on=None, rejected_on=None).order_by('id')

    @property
    def is_pending(self):
        return self.approved_on is None and self.rejected_on is None

    @property
    def is_approved(self):
        return self.approved_on is not None and self.rejected_on is None

    @property
    def is_rejected(self):
        return self.rejected_on is not None

    @property
    def status(self):
        if self.is_pending:
            return "Pending approval"
        elif self.is_approved:
            return "Approved on {}".format(self.approved_on.date())
        elif self.is_rejected:
            return "Rejected on {}".format(self.rejected_on.date())
        else:
            raise ValueError("Unknown state")

    def approve(self):
        if self.approved_on is not None:
            raise ValueError('The membership has already been approved')

        self.approved_on = timezone.now()
        self.save()

        # Send a confirmation email
        context = email_context(membership=self)
        self.user_profile.send_email('Your application has been approved!',
                                     render_to_string('gui/email/application_accepted_message.txt',
                                                      context))

    def reject(self, reason):
        if self.rejected_on is not None:
            raise ValueError('The membership has already been rejected')
        if reason is None or len(reason) == 0:
            raise ValueError('A membership cannot be rejected without a reason')

        self.rejected_on = timezone.now()
        self.rejection_reason = reason
        self.save()

        # Send a rejection email
        context = email_context(membership=self)
        self.user_profile.send_email('Your application has been rejected...',
                                     render_to_string('gui/email/application_rejected_message.txt',
                                                      context))

    def __str__(self):
        return "{}'s membership for the period {} ({})".format(self.user_profile, self.period.short_name,
                                                               self.status)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    memberships = models.ManyToManyField(MembershipPeriod, through="Membership")

    affiliation = models.CharField(max_length=80, blank=True,
                                   help_text="Current affiliation (leave empty for hobbyists or "
                                             "if you want to keep this private)")
    public_statement = models.TextField(blank=True,
                                        help_text="This information will be displayed to other members")

    @classmethod
    def existing_affiliations(cls):
        return sorted(set([e[0] for e in Profile.objects.values_list('affiliation') if len(e[0]) > 0]))

    @receiver(post_save, sender=User)
    def create_member(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_member(sender, instance, **kwargs):
        instance.profile.save()

    def send_email(self, subject, message):
        Email(subject, message, [self.user.email]).send()

    @cached_property
    def membership(self):
        cur_period = MembershipPeriod.current_period()
        if cur_period is None:
            return None
        return Membership.objects.filter(user_profile=self, period=cur_period).first()

    @cached_property
    def last_membership(self):
        return self.memberships.exclude(membership__approved_on=None).last()

    @cached_property
    def active_ballots(self):
        return [(b, b.has_voted(self.membership)) for b in Ballot.active_ballots()]

    def __str__(self):
        return "{} <{}>".format(self.user.get_full_name(), self.user.email)


class VoteCount:
    def __init__(self, count, total):
        if count > total:
            raise ValueError("The count of votes cannot be greater than the total amount of votes")

        self.count = count
        self.total = total

    @property
    def percentage(self):
        if self.total > 0:
            return self.count * 100.0 / self.total
        else:
            return 0

    def __eq__(self, other):
        return self.count == other.count and self.total == other.total

    def __repr__(self):
        return "VoteCount({}, {})".format(self.count, self.total)

    def __str__(self):
        return "{:.1f}% ({} / {})".format(self.percentage, self.count, self.total)


class Ballot(models.Model):
    short_name = models.CharField(max_length=100, unique=True,
                                  help_text="Short name to describe the ballot")
    description = models.TextField(blank=True, null=True,
                                   help_text="Longer description of the ballot containing all the "
                                             "relevant information for members to use to vote.")

    created_on = models.DateTimeField(auto_now_add=True)
    opening_on = models.DateTimeField()
    closing_on = models.DateTimeField()

    voters = models.ManyToManyField(Membership, help_text="List of members who voted on this ballot")

    @classmethod
    def active_ballots(self):
        return Ballot.objects.filter(opening_on__lte=timezone.now(), closing_on__gt=timezone.now())

    @cached_property
    def period(self):
        return MembershipPeriod.active_period_at(self.created_on)

    @cached_property
    def potential_voters(self):
        if self.period is None:
            return set()

        return set(self.period.members.exclude(approved_on__gt=self.closing_on))

    @cached_property
    def missing_voters(self):
        return self.potential_voters - set(self.voters.all())

    @property
    def is_active(self):
        now = timezone.now()
        return now >= self.opening_on and now < self.closing_on

    @property
    def has_started(self):
        return timezone.now() > self.opening_on

    @property
    def has_closed(self):
        return timezone.now() > self.closing_on

    def has_voted(self, membership):
        return self.voters.filter(pk=membership.pk).exists()

    @cached_property
    def turnout(self):
        if self.period is None:
            return VoteCount(0, 0)

        total = len(self.potential_voters)
        count = self.voters.all().count()
        return VoteCount(count, total)

    def send_reminder(self):
        title = "Please cast your vote for the '{}' vote".format(self.short_name)
        message = render_to_string('gui/email/ballot_reminder_message.txt', email_context(ballot=self))

        for member in self.missing_voters:
            member.user_profile.send_email(title, message)

    def __str__(self):
        return "Ballot<{}>".format(self.short_name)


# Answer selection
class BallotSelectQuestion(models.Model):
    ballot = models.ForeignKey('Ballot', on_delete=models.CASCADE, related_name="select_questions")

    question = models.CharField(max_length=255, blank=True, null=True,
                                help_text="Question asking members to select one in multiple answers")

    @cached_property
    def tally(self):
        if not self.ballot.has_closed:
            raise ValueError("No tally can be performed until the ballot has closed")

        ret = OrderedDict()

        total_votes = self.selected_answers.count()
        possible_answers = BallotSelectQuestionPossibleAnswer.objects.filter(question=self)
        for answer in possible_answers.annotate(vote_count=Count('votes')).order_by('-vote_count'):
            ret[answer] = VoteCount(answer.vote_count, total_votes)

        return ret

    def __str__(self):
        return "{}: {}".format(self.ballot, self.question)


class BallotSelectQuestionPossibleAnswer(models.Model):
    question = models.ForeignKey(BallotSelectQuestion, on_delete=models.CASCADE, related_name="possible_answers")

    answer = models.CharField(max_length=100, help_text="One possible answer")

    def __str__(self):
        return "{}: {}".format(self.question, self.answer)


class BallotSelectedAnswer(models.Model):
    question = models.ForeignKey(BallotSelectQuestion, on_delete=models.CASCADE, related_name="selected_answers")
    answer = models.ForeignKey(BallotSelectQuestionPossibleAnswer, on_delete=models.CASCADE,
                               null=True, related_name="votes")
    member = models.ForeignKey(Membership, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('question', 'member')

    def __str__(self):
        return "{} - '{}' selected '{}'".format(self.question, self.member.user_profile.user.email, self.answer.answer)


# Ranking selection
class OptionRankingVotes:
    def __init__(self, ranking_option, option_count):
        self.ranking_option = ranking_option
        self.option_count = option_count

        # Initialize an array indexed by the rank with 0s
        self.count_by_rank = [0] * option_count
        for e in ranking_option.votes.values('rank').annotate(count=Count('rank')):
            self.count_by_rank[e['rank'] - 1] = e['count']

    @cached_property
    def score(self):
        score = 0
        for rank, count in enumerate(self.count_by_rank):
            score += (self.option_count - rank) * count
        return score

    def __str__(self):
        return "{} - score={}".format(self.ranking_option, self.score)


class BallotRankingQuestion(models.Model):
    ballot = models.ForeignKey('Ballot', on_delete=models.CASCADE, related_name="ranking_questions")

    question = models.CharField(max_length=255, blank=True, null=True,
                                help_text="Question asking members to rank multiple options")

    @cached_property
    def tally(self):
        if not self.ballot.has_closed:
            raise ValueError("No tally can be performed until the ballot has closed")

        p_opts = self.possible_options.all()
        options = [OptionRankingVotes(o, len(p_opts)) for o in p_opts]
        return sorted(options, key=lambda o: o.score, reverse=True)

    def __str__(self):
        return "{}: {}".format(self.ballot, self.question)


class BallotRankingQuestionOption(models.Model):
    question = models.ForeignKey(BallotRankingQuestion, on_delete=models.CASCADE, related_name="possible_options")

    option = models.CharField(max_length=100, help_text="One option to be ranked")

    def __str__(self):
        return "{}: {}".format(self.question, self.option)


class BallotRankingAnswer(models.Model):
    question = models.ForeignKey(BallotRankingQuestion, on_delete=models.CASCADE)
    member = models.ForeignKey(Membership, on_delete=models.CASCADE)

    option = models.ForeignKey(BallotRankingQuestionOption, on_delete=models.CASCADE, related_name="votes",
                               blank=True, null=True)
    rank = models.PositiveSmallIntegerField(help_text="Rank this option got", default=0)

    class Meta:
        unique_together = ('question', 'member', 'rank')

    def __str__(self):
        if self.option is not None:
            return "{}: '{}' - '{}' was ranked {}".format(self.question, self.member.user_profile.user.email,
                                                          self.option.option, ordinal(self.rank))
        else:
            return "{}: '{}' - abstained for {} rank".format(self.question, self.member.user_profile.user.email,
                                                             ordinal(self.rank))
