from allauth.account.decorators import verified_email_required
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import UserPassesTestMixin
from django.utils.safestring import mark_safe
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.template.loader import render_to_string
from django.views import View
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, FormView
from django.views.generic.list import ListView

from .email import AdminEmail, email_context
from .models import Link, MembershipPeriod, Membership, Ballot
from .forms import ProfileForm, MembershipApplicationForm, BallotForm


class RequiresValidMembershipMixin(UserPassesTestMixin):
    def test_func(self):
        try:
            membership = self.request.user.profile.membership
            return membership is not None and membership.is_approved
        except AttributeError:
            # We have an anonymous user, don't give access!
            return False


def index(request):
    context = {
        "links": Link.objects.all().order_by("index_position"),
    }
    return render(request, 'gui/index.html', context)


@verified_email_required
def profile(request):
    user = request.user

    if request.method == 'POST':
        form = ProfileForm(request.POST)

        if form.is_valid():
            form.save(user)
            messages.add_message(request, messages.SUCCESS, 'Your profile was updated successfully')
            return HttpResponseRedirect(reverse('index'))
        else:
            messages.add_message(request, messages.ERROR, mark_safe('Your profile is invalid: {}'.format(form.errors)))
            return HttpResponseRedirect(reverse('account-profile'))
    else:
        form = ProfileForm(initial={"first_name": user.first_name,
                                    "last_name": user.last_name,
                                    "affiliation": user.profile.affiliation,
                                    "public_statement": user.profile.public_statement})

    return render(request, 'gui/profile.html', {'form': form})


@verified_email_required
def account_delete(request):
    user = request.user

    if request.method == 'POST':
        user.delete()
        messages.add_message(request, messages.SUCCESS, 'Your account has been deleted successfully')
        return HttpResponseRedirect(reverse('index'))
    else:
        return render(request, 'gui/account_delete.html', {})


@verified_email_required
def membership_application(request):
    user = request.user

    cur_period = MembershipPeriod.current_period()
    if cur_period is None:
        msg = ("No membership period has been created. If you are an administrator, please create a "
               "period to allow members to apply.")
        return render(request, 'gui/message.html', {"title": "Membership Application",
                                                    "severity": "warning",
                                                    "panel_title": "Can't apply when no membership period exists",
                                                    "message": msg})

    # Do not allow re-applying if an application exists
    if user.profile.membership is not None:
        msg = "You already applied to the period {}. Current status: {}".format(cur_period.short_name,
                                                                                user.profile.membership.status)
        title = "ERROR: An application for the period {} already exists for your user".format(cur_period.short_name)
        return render(request, 'gui/message.html', {"title": "Membership Application",
                                                    "severity": "danger",
                                                    "panel_title": title,
                                                    "message": msg})

    if request.method == 'POST':
        form = MembershipApplicationForm(request.POST)

        if form.is_valid() and form.cleaned_data.get('user_id') == user.id:
            membership = form.save()

            context = email_context(new_membership=membership)
            AdminEmail(render_to_string('gui/email/new_membership_application_subject.txt',
                                        context),
                       render_to_string('gui/email/new_membership_application_message.txt',
                                        context)).send()

            messages.add_message(request, messages.SUCCESS,
                                 'Your application was sent successfully and will be reviewed shortly')

            return HttpResponseRedirect(reverse('index'))
        messages.add_message(request, messages.ERROR, mark_safe('Your application is invalid'))
        return HttpResponseRedirect(reverse('membership-application'))
    else:
        form = MembershipApplicationForm(initial={"period_id": cur_period.id,
                                                  "user_id": user.id,
                                                  "public_statement": user.profile.public_statement,
                                                  "agree_membership": False})

    return render(request, 'gui/membership_application.html', {'form': form})


class RequiresSuperUserMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_superuser


class ChangeUserStatus(RequiresSuperUserMixin, View):
    def post(self, request, pk, superuser):
        user = get_object_or_404(get_user_model(), pk=pk)
        if user != self.request.user:
            role = "administrator" if user.is_superuser else "user"
            new_role = "administrator" if superuser else "user"

            # Make the change
            user.is_superuser = user.is_staff = superuser
            user.save()

            # Send an email to the user about the status change
            context = email_context(role=role, new_role=new_role, admin=self.request.user)
            user.profile.send_email("Your status changed from '{}' to '{}'".format(role, new_role),
                                    render_to_string('gui/email/user_role_change_message.txt',
                                                     context))

            messages.add_message(request, messages.SUCCESS,
                                 "The member '{}' has been made {}".format(user.profile,
                                                                           new_role))
        else:
            messages.add_message(request, messages.ERROR,
                                 "You cannot change your own access rights")
        return HttpResponseRedirect(request.META.get("HTTP_REFERER", reverse('index')))


@method_decorator(verified_email_required, name='dispatch')
class MembershipPeriodCreateView(RequiresSuperUserMixin, CreateView):
    model = MembershipPeriod
    fields = ['short_name', 'description', 'agreement_url', 'start', 'end']
    template_name = "gui/membership_period_create.html"

    def get_success_url(self):
        return reverse('index')

    def get_initial(self):
        initial = super().get_initial()

        cur_period = MembershipPeriod.current_period()
        if cur_period is not None:
            initial['short_name'] = cur_period.short_name
            initial['description'] = cur_period.description
            initial['agreement_url'] = cur_period.agreement_url
            initial['start'] = cur_period.start

        return initial


@method_decorator(verified_email_required, name='dispatch')
class MembershipApplicationListView(RequiresSuperUserMixin, ListView):
    model = Membership
    paginate_by = 10
    template_name = "gui/membership_approval.html"

    def get_queryset(self):
        return Membership.pending_memberships()

    def post(self, request, *args, **kwargs):
        params = request.POST

        try:
            ids = [int(id) for id in params.getlist('ids', [])]
            memberships = Membership.objects.filter(id__in=ids)

            if params.get("action") == "approve":
                for membership in memberships:
                    membership.approve()
                messages.add_message(request, messages.SUCCESS,
                                     'You approved {} membership applications'.format(len(memberships)))
            elif params.get("action") == "reject":
                rejected = []
                for membership in memberships:
                    reason = params.get("reject_reason_{}".format(membership.id))
                    if reason is not None and len(reason) > 0:
                        membership.reject(reason)
                        rejected.append(membership)
                    else:
                        msg = "Can't refuse {}'s application without a reason".format(membership.user_profile)
                        messages.add_message(request, messages.ERROR, msg)

                if len(rejected) > 0:
                    messages.add_message(request, messages.SUCCESS,
                                         'You rejected {} membership applications'.format(len(rejected)))

            else:
                messages.add_message(request, messages.ERROR,
                                     "ERROR: Unsupported action...")
        except ValueError:
            messages.add_message(request, messages.ERROR,
                                 "ERROR: One or more IDs are not integers...")

        return HttpResponseRedirect(reverse('membership-approval'))


class MembersListView(RequiresValidMembershipMixin, ListView):
    model = Membership
    template_name = "gui/members.html"

    def get_queryset(self):
        period = MembershipPeriod.current_period()
        if period is not None:
            query = period.members.order_by("user_profile__user__last_name")
            return query.select_related('user_profile', 'user_profile__user')
        else:
            return []  # pragma: no cover  (we cannot have a valid membership and but no period)


def about(request):
    return render(request, 'gui/about.html', {})


@method_decorator(verified_email_required, name='dispatch')
class BallotListView(RequiresSuperUserMixin, ListView):
    model = Ballot


class VotingView(RequiresValidMembershipMixin, FormView):
    template_name = 'gui/vote.html'
    form_class = BallotForm

    def get_ballot(self):
        ballot = get_object_or_404(Ballot, pk=self.kwargs['pk'])

        # TODO: Use message.html to describe the issue rather than just dying out
        if timezone.now() < ballot.opening_on:
            raise Http404('{} is not yet open'.format(ballot))
        elif timezone.now() > ballot.closing_on:
            raise Http404('{} is already closed'.format(ballot))
        elif self.request.user.profile.membership in ballot.voters.all():
            raise Http404('{} has already voted in the ballot'.format(self.request.user.profile.membership))
        return ballot

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['ballot'] = self.get_ballot()
        return kwargs

    def get_success_url(self):
        return reverse('index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ballot'] = self.get_ballot()
        return context

    def form_valid(self, form):
        # Since the form is valid, add the results of the vote to the DB
        # NOTICE: the membership has to be valid, thanks to RequiresValidMembershipMixin
        form.save(self.request.user.profile.membership)
        messages.add_message(self.request, messages.SUCCESS, "Thanks for casting a vote!")

        return super().form_valid(form)


@method_decorator(verified_email_required, name='dispatch')
class BallotAdminView(RequiresSuperUserMixin, DetailView):
    model = Ballot
    template_name = 'gui/ballot_admin.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ballot'] = context.pop('object')
        return context


class BallotSendReminderView(RequiresSuperUserMixin, View):
    def post(self, request, pk):
        ballot = get_object_or_404(Ballot, pk=pk)
        ballot.send_reminder()

        messages.add_message(request, messages.SUCCESS,
                             "An email has been sent to all members who have not voted yet")
        return HttpResponseRedirect(request.META.get("HTTP_REFERER", reverse('index')))
