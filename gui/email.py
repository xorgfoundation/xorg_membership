from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site


class Email:
    def __init__(self, subject, message, to):
        self.subject = subject.strip()
        self.message = message
        self.to = to

    def send(self):
        subject = "{} {}".format('[X.Org membership]', self.subject)

        from_email = settings.DEFAULT_FROM_EMAIL
        if from_email is None:
            from_email = "{}@{}".format(settings.EMAIL_HOST_USER, settings.EMAIL_HOST)

        send_mail(subject, self.message, from_email, self.to)


class AdminEmail(Email):
    def __init__(self, subject, message):
        to = [u.email for u in get_user_model().objects.filter(is_superuser=True)]
        super().__init__(subject, message, to)


def email_context(**kwargs):
    kwargs['current_site'] = Site.objects.get_current()
    return kwargs
