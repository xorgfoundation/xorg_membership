from django import forms
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.utils.functional import cached_property

from .models import Membership, MembershipPeriod, Profile
from .models import BallotSelectQuestionPossibleAnswer, BallotRankingQuestionOption
from .models import BallotRankingAnswer, BallotSelectedAnswer


# Origin: https://stackoverflow.com/a/32791625
class ListTextWidget(forms.TextInput):
    def __init__(self, data_list, name, *args, **kwargs):
        super(ListTextWidget, self).__init__(*args, **kwargs)
        self._name = name
        self._list = data_list
        self.attrs.update({'list': 'list__%s' % self._name})

    def render(self, name, value, attrs=None, renderer=None):
        text_html = super(ListTextWidget, self).render(name, value, attrs=attrs)
        data_list = '<datalist id="list__%s">' % self._name
        for item in self._list:
            data_list += '<option value="%s">' % item
        data_list += '</datalist>'

        return (text_html + data_list)


class ProfileForm(forms.Form):
    first_name = forms.CharField(label='First Name', max_length=100)
    last_name = forms.CharField(label='Last Name', max_length=100)

    affiliation = forms.CharField(label='Affiliation', help_text="Leave empty for hobbyists",
                                  max_length=80, required=False)
    public_statement = forms.CharField(widget=forms.Textarea, label="Public Statement",
                                       help_text="This information will be shown only to other members and admins")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Override the widget now to ensure the list is fetched every time this object is created
        self.fields['affiliation'].widget = ListTextWidget(data_list=Profile.existing_affiliations(),
                                                           name='affiliations-list')

    def save(self, user):
        if self.is_valid():
            user.first_name = self.cleaned_data['first_name']
            user.last_name = self.cleaned_data['last_name']
            user.profile.affiliation = self.cleaned_data['affiliation']
            user.profile.public_statement = self.cleaned_data['public_statement']
            user.save()


class MembershipApplicationForm(forms.Form):
    period_id = forms.IntegerField(required=True, widget=forms.HiddenInput())

    user_id = forms.IntegerField(required=True, widget=forms.HiddenInput())

    public_statement = forms.CharField(widget=forms.Textarea, label="Public Statement",
                                       help_text="Update your profile's public statement, which will be "
                                                 "used by administrators to accept/deny your application "
                                                 "and will be seen by other members")

    agree_membership = forms.BooleanField(label="I have read and agreed with the membership agreement", required=True)

    def save(self):
        if self.is_valid() and self.cleaned_data['agree_membership'] is True:
            user = get_object_or_404(User, pk=self.cleaned_data['user_id'])
            period = get_object_or_404(MembershipPeriod, pk=self.cleaned_data['period_id'])

            # Update the user profile
            user.profile.public_statement = self.cleaned_data['public_statement']
            user.save()

            return Membership.objects.create(period=period, user_profile=user.profile)
        else:
            return None


class OptionRankingSubField(forms.Select):
    template_name = 'gui/widgets/option_ranking_subfield.html'

    def __init__(self, rank, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.rank = rank

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['rank'] = self.rank
        return context


class OptionRankingWidget(forms.MultiWidget):
    # NOTICE: the attribute `field` should be set by the MultiValueField
    # referencing this widget. See OptionRankingField.__createMultiWidget.
    def __init__(self, attr=None):
        widgets = []
        for i, option in enumerate(self.field.options):
            # Only add non-abstain
            if option[0] >= 0:
                widgets.append(OptionRankingSubField(rank=i+1, choices=self.field.select_choices))

        if attr is None:  # pragma: nocover
            attr = {}
        attr["data-ranking-id"] = self.field.ranking.pk

        super().__init__(widgets, attr)

    def decompress(self, value):
        if value:
            return value
        return [None] * len(self.widgets)


class OptionRankingField(forms.MultiValueField):
    @cached_property
    def options(self):
        return [(o.id, o.option) for o in self.ranking.possible_options.all()] + [(-1, '-- Abstain --')]

    @cached_property
    def select_choices(self):
        return [('', '-- Select an option --')] + self.options

    def __createMultiWidget(self):
        class C(OptionRankingWidget):
            field = self
        return C

    def __init__(self, ranking, *args, **kwargs):
        self.ranking = ranking

        fields = []
        for i, option in enumerate(self.options):
            if option[0] >= 0:
                fields.append(forms.ChoiceField(required=True, choices=self.options, label=str(i)))

        self.widget = self.__createMultiWidget()
        super().__init__(fields=fields, *args, **kwargs)

    def compress(self, data_list):
        return data_list


class BallotForm(forms.Form):
    def __init__(self, *args, **kwargs):
        try:
            self.ballot = kwargs.pop('ballot')
        except KeyError:
            self.ballot = None

        super().__init__(*args, **kwargs)

        if self.ballot is None:
            return

        self.ranking_questions = dict()
        for i, ranking in enumerate(self.ballot.ranking_questions.all()):
            ranking_field = OptionRankingField(ranking, required=True,
                                               label=ranking.question)
            field_name = "ranking_{}".format(i)
            self.fields[field_name] = ranking_field
            self.ranking_questions[ranking] = field_name

        # Construct the form based on the questions found in the ballot. Start with the select questions
        self.select_questions = dict()
        for i, select in enumerate(self.ballot.select_questions.all()):
            fixed_answers = [('', '-- Select an answer --')]
            answers = fixed_answers + [(a.id, a.answer) for a in select.possible_answers.all()]
            select_field = forms.ChoiceField(required=True, choices=answers,
                                             label=select.question)
            field_name = "select_{}".format(i)
            self.fields[field_name] = select_field
            self.select_questions[select] = field_name

    def _get_objects(self, name, Model, ids, fail_on_duplicates=True):
        try:
            ids = [int(id) for id in ids]
        except ValueError:
            self.add_error(name, "The selected ids are not integer")
            return None

        ids_without_abstain = [i for i in ids if i >= 0]
        if fail_on_duplicates and len(set(ids_without_abstain)) != len(ids_without_abstain):
            self.add_error(name, "Options cannot be duplicated")
            return None

        objects = []
        has_abstains = False
        for obj_id in ids:
            obj = Model.objects.filter(pk=obj_id).first()
            if obj_id >= 0:
                # This is a non-abstain option
                if has_abstains:
                    self.add_error(name, "Only the lowest-ranking options can be abstained")
                    return None
                elif obj is None:
                    self.add_error(name, "At least one selected item is invalid")
                    return None
            else:
                # This is an abstain option
                has_abstains = True

            objects.append(obj)

        return objects

    def clean(self):
        cleaned_data = super().clean()

        for name in list(cleaned_data.keys()):
            if name.startswith('select_'):
                objs = self._get_objects(name, BallotSelectQuestionPossibleAnswer, [cleaned_data[name]])
                if objs is not None and len(objs) == 1:
                    cleaned_data[name] = objs[0]
                else:
                    # This case cannot happen because the form is already checking
                    # that the value returned is one we generated
                    pass  # pragma: no cover
            elif name.startswith('ranking_'):
                cleaned_data[name] = self._get_objects(name, BallotRankingQuestionOption, cleaned_data[name])
            else:
                # Not a possible case since the super().clean already scrubbed the fieldnames
                pass  # pragma: no cover

        return cleaned_data

    @transaction.atomic
    def save(self, member):
        if not self.is_valid():
            return False

        # Create all the ranking answers
        for question, field in self.ranking_questions.items():
            for rank, option in enumerate(self.cleaned_data[field]):
                BallotRankingAnswer.objects.create(question=question, member=member, option=option, rank=rank + 1)

        # Create all the select answers
        for question, field in self.select_questions.items():
            answer = self.cleaned_data[field]
            BallotSelectedAnswer.objects.create(question=question, member=member, answer=answer)

        # Finally, say that the member has voted!
        self.ballot.voters.add(member)

        return True
