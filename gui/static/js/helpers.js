function handleSelectAll() {
    /* Add a checkbox */
    $('[data-select-name]').each(function(){
        $(this).prepend('<span class="select-item"><input type="checkbox" name="' + $(this).data("select-name") + '"/></span>');
    });

    $('a[data-select-all]').click(function() {
        $("#" + $(this).data("select-all")).find("input").prop('checked', true);
        return false;
    });

    $('a[data-unselect-all]').click(function() {
        $("#" + $(this).data("unselect-all")).find("input").prop('checked', false);
        return false;
    });
}

function hideSelectedOptionsInRankingQuestions() {
    function update() {
        /* First, find all the selected values for all the questions */
        var selected_values_per_ranking_id = {};
        $('select[data-ranking-id]').each(function() {
            var ranking_id = $(this).data("ranking-id");
            var selected_val = parseInt($(this).children("option:selected").val());

            /* ignore anything that is not a number, like empty strings, or abstains */
            if (isNaN(selected_val) || selected_val < 0 )
                return;

            /* add the selected value to the associated question */
            if (ranking_id in selected_values_per_ranking_id)
                var selected_values = selected_values_per_ranking_id[ranking_id]
            else
                var selected_values = new Set();

            selected_values.add(selected_val)
            selected_values_per_ranking_id[ranking_id] = selected_values
        });

        /* Mark all the selected values as disabled in all the selects that are
        * not already selecting it
        */
        for (var ranking_id in selected_values_per_ranking_id) {
            $('select[data-ranking-id='+ranking_id+']').each(function() {
                $(this).children("option:not(:selected)").each(function() {
                    var option_val = parseInt($(this).val());
                    console.log(option_val, selected_values_per_ranking_id[ranking_id], selected_values_per_ranking_id[ranking_id].has(option_val))

                    if (selected_values_per_ranking_id[ranking_id].has(option_val))
                        $(this).attr('disabled', 'disabled')
                    else
                        $(this).removeAttr('disabled')
                });
            });
        }
    }

    $('select[data-ranking-id]').click(function() {
        update();
    });

    /* Make sure that anything already selected is taken into account */
    update();
}

/* Handle all the start-up scripts */
$(document).ready(function() {
    handleSelectAll();
    hideSelectedOptionsInRankingQuestions();
})
