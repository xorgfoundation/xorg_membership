from .models import MembershipPeriod, Membership
import os


def admin_tasks_count(request):
    user = request.user

    admin_tasks_count = 0
    if user.is_superuser:
        admin_tasks_count = Membership.pending_memberships().count()

    return {"admin_tasks_count": admin_tasks_count}


class Website:
    def __init__(self, request):
        self.request = request

    @property
    def version(self):
        return os.getenv('WEBSITE_VERSION', None)

    @property
    def project_url(self):
        return "https://gitlab.freedesktop.org/xorgfoundation/xorg_membership"

    @property
    def version_url(self):
        return "{}/-/commit/{}".format(self.project_url, self.version)


def global_context(request):
    return {
        "current_membership_period": MembershipPeriod.current_period(),
        "website": Website(request)
    }
